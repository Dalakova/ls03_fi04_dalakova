import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

public class UI_Copy extends JFrame {

	private JPanel contentPane;
	private JTextField txtText;
	private String text = "Dieser Text soll ver�ndert werden.";
	private int textFontSize = 14;
	private Font textFont = new Font("Tahoma", Font.PLAIN, textFontSize);
	private int titleFontSize = 14;
	private Font titleFont = new Font("Tahoma", Font.BOLD, titleFontSize);
	private int btnHeight = 30;
	private JTextField txtAufgabe1;
	private JTextField txtAufgabe2;
	private JTextField txtNewText;
	private int btnFontSize = 12;
	private Font btnFont = new Font("Tahoma", Font.BOLD, btnFontSize);
	private JTextField txtAufgabe3;
	private JTextField txtAufgabe4;
	private JTextField txtAufgabe5;
	private JTextField txtAufgabe6;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UI_Copy frame = new UI_Copy();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UI_Copy() {
		setResizable(false);
		setTitle("UI_Copy");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 460 + 8*btnHeight);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//text field
		txtText =  new JTextField();
		txtText.setEditable(false);
		txtText.setFont(textFont);
		txtText.setForeground(Color.BLACK);
		txtText.setHorizontalAlignment(SwingConstants.CENTER);
		txtText.setText(text);
		txtText.setBorder(null);
		txtText.setBackground(null);
		txtText.setBounds(10, 10, 410, 100);
		contentPane.add(txtText);
		txtText.setColumns(10);
		
		//title for Aufgabe 1
		txtAufgabe1 = new JTextField();
		txtAufgabe1.setText("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		txtAufgabe1.setHorizontalAlignment(SwingConstants.LEFT);
		txtAufgabe1.setForeground(Color.BLACK);
		txtAufgabe1.setFont(titleFont);
		txtAufgabe1.setEditable(false);
		txtAufgabe1.setBorder(null);
		txtAufgabe1.setBackground(null);
		txtAufgabe1.setColumns(10);
		txtAufgabe1.setBounds(10, 110, 410, 30);
		contentPane.add(txtAufgabe1);
		
		//button to change background to red
		JButton btnRed = new JButton("Rot");
		btnRed.setFont(btnFont);
		btnRed.setBackground(UIManager.getColor("Button.background"));
		btnRed.setBounds(10, 140, 130, btnHeight);
		contentPane.add(btnRed);
		btnRed.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                UI_Copy.this.btnRedAction(evt);
            }
        });	
		//button to change background to green
		JButton btnGreen = new JButton("Gr�n");
		btnGreen.setFont(btnFont);
		btnGreen.setBackground(UIManager.getColor("Button.background"));
		btnGreen.setBounds(150, 140, 130, btnHeight);
		contentPane.add(btnGreen);
		btnGreen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnGreenAction(evt);
			}
		});
		//button to change background to blue
		JButton btnBlue = new JButton("Blau");
		btnBlue.setFont(btnFont);
		btnBlue.setBackground(UIManager.getColor("Button.background"));
		btnBlue.setBounds(290, 140, 130, btnHeight);
		contentPane.add(btnBlue);
		btnBlue.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnBlueAction(evt);
			}
		});
		//button to change background to red
		JButton btnYellow = new JButton("Gelb");
		btnYellow.setFont(btnFont);
		btnYellow.setBackground(UIManager.getColor("Button.background"));
		btnYellow.setBounds(10, 150 + btnHeight, 130, btnHeight);
		contentPane.add(btnYellow);
		btnYellow.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnYellowAction(evt);
			}
		});	
		//button to change background to standard
		JButton btnStandard = new JButton("Standardfarbe");
		btnStandard.setFont(btnFont);
		btnStandard.setBackground(UIManager.getColor("Button.background"));
		btnStandard.setBounds(150, 150 + btnHeight, 130, btnHeight);
		contentPane.add(btnStandard);
		btnStandard.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnStandardAction(evt);
			}
		});
		//button to change background to chosen color
		JButton btnChooseColor = new JButton("Farbe w�hlen");
		btnChooseColor.setFont(btnFont);
		btnChooseColor.setBackground(UIManager.getColor("Button.background"));
		btnChooseColor.setBounds(290, 150 + btnHeight, 130, btnHeight);
		contentPane.add(btnChooseColor);
		btnChooseColor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnChooseColorAction(evt);
			}
		});	
		
		//title for Aufgabe 2
		txtAufgabe2 = new JTextField();
		txtAufgabe2.setText("Aufgabe 2: Text formatieren");
		txtAufgabe2.setHorizontalAlignment(SwingConstants.LEFT);
		txtAufgabe2.setForeground(Color.BLACK);
		txtAufgabe2.setFont(titleFont);
		txtAufgabe2.setEditable(false);
		txtAufgabe2.setBorder(null);
		txtAufgabe2.setBackground(null);
		txtAufgabe2.setColumns(10);
		txtAufgabe2.setBounds(10, 160 + 2*btnHeight, 410, 30);
		contentPane.add(txtAufgabe2);
		
		//button to change a font of the text to Arial
		JButton btnArial = new JButton("Arial");
		btnArial.setFont(btnFont);
		btnArial.setBackground(UIManager.getColor("Button.background"));
		btnArial.setBounds(10, 190 + 2*btnHeight, 130, btnHeight);
		contentPane.add(btnArial);
		btnArial.addActionListener(new ActionListener() {
			@Override
		    public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnArialAction(evt);
		    }
		});
		//button to change a font of the text to Comic Sans NS
		JButton btnComicsans = new JButton("Comic Sans MS");
		btnComicsans.setFont(btnFont);
		btnComicsans.setBackground(UIManager.getColor("Button.background"));
		btnComicsans.setBounds(150, 190 + 2*btnHeight, 130, btnHeight);
		contentPane.add(btnComicsans);
		btnComicsans.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnComicsansAction(evt);
			}
		});
		//button to change a font of the text to Courier New
		JButton btnCourier = new JButton("Courier New");
		btnCourier.setFont(btnFont);
		btnCourier.setBackground(UIManager.getColor("Button.background"));
		btnCourier.setBounds(290, 190 + 2*btnHeight, 130, btnHeight);
		contentPane.add(btnCourier);
		btnCourier.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnCourierAction(evt);
			}
		});
		
		//field to change text
		txtNewText =  new JTextField();
		txtNewText.setEditable(true);
		txtNewText.setFont(textFont);
		txtNewText.setForeground(Color.BLACK);
		txtNewText.setHorizontalAlignment(SwingConstants.LEFT);
		txtNewText.setText("Hier bitte Text eingeben");
		txtNewText.setBorder(new LineBorder(new Color(0, 0, 0)));
		txtNewText.setBackground(Color.WHITE);
		txtNewText.setBounds(10, 200 + 3*btnHeight, 410, 30);
		contentPane.add(txtNewText);
		txtNewText.setColumns(10);
		
		//button to change a text
		JButton btnChangeText = new JButton("Ins Label schreiben");
		btnChangeText.setFont(btnFont);
		btnChangeText.setBackground(UIManager.getColor("Button.background"));
		btnChangeText.setBounds(10, 240 + 3*btnHeight, 200, btnHeight);
		contentPane.add(btnChangeText);
		btnChangeText.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnChangeTextAction(evt);
			}
		});
		//button to delete a text
		JButton btnDeleteText = new JButton("Text im Label l�schen");
		btnDeleteText.setFont(btnFont);
		btnDeleteText.setBackground(UIManager.getColor("Button.background"));
		btnDeleteText.setBounds(220, 240 + 3*btnHeight, 200, btnHeight);
		contentPane.add(btnDeleteText);
		btnDeleteText.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnDeleteTextAction(evt);
			}
		});
		
		//title for Aufgabe 3
		txtAufgabe3 = new JTextField();
		txtAufgabe3.setText("Aufgabe 3: Schriftfarbe �ndern");
		txtAufgabe3.setHorizontalAlignment(SwingConstants.LEFT);
		txtAufgabe3.setForeground(Color.BLACK);
		txtAufgabe3.setFont(titleFont);
		txtAufgabe3.setEditable(false);
		txtAufgabe3.setBorder(null);
		txtAufgabe3.setBackground(null);
		txtAufgabe3.setColumns(10);
		txtAufgabe3.setBounds(10, 250 + 4*btnHeight, 410, 30);
		contentPane.add(txtAufgabe3);
		
		//button to change a text to red
		JButton btnRedText = new JButton("Rot");
		btnRedText.setFont(btnFont);
		btnRedText.setBackground(UIManager.getColor("Button.background"));
		btnRedText.setBounds(10, 280 + 4*btnHeight, 130, btnHeight);
		contentPane.add(btnRedText);
		btnRedText.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                UI_Copy.this.btnRedTextAction(evt);
            }
        });	
		//button to change a text to blue
		JButton btnBlueText = new JButton("Blau");
		btnBlueText.setFont(btnFont);
		btnBlueText.setBackground(UIManager.getColor("Button.background"));
		btnBlueText.setBounds(150, 280 + 4*btnHeight, 130, btnHeight);
		contentPane.add(btnBlueText);
		btnBlueText.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnBlueTextAction(evt);
			}
		});
		//button to change a text to black
		JButton btnBlackText = new JButton("Schwarz");
		btnBlackText.setBackground(UIManager.getColor("Button.background"));
		btnBlackText.setBounds(290, 280 + 4*btnHeight, 130, btnHeight);
		contentPane.add(btnBlackText);
		btnBlackText.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnBlackTextAction(evt);
			}
		});
		
		//title for Aufgabe 4
		txtAufgabe4 = new JTextField();
		txtAufgabe4.setText("Aufgabe 4: Schriftgr��e ver�ndern");
		txtAufgabe4.setHorizontalAlignment(SwingConstants.LEFT);
		txtAufgabe4.setForeground(Color.BLACK);
		txtAufgabe4.setFont(titleFont);
		txtAufgabe4.setEditable(false);
		txtAufgabe4.setBorder(null);
		txtAufgabe4.setBackground(null);
		txtAufgabe4.setColumns(10);
		txtAufgabe4.setBounds(10, 290 + 5*btnHeight, 410, 30);
		contentPane.add(txtAufgabe4);
		
		//button to increase a size of the text font
		JButton btnPlusFont = new JButton("+");
		btnPlusFont.setFont(btnFont);
		btnPlusFont.setBackground(UIManager.getColor("Button.background"));
		btnPlusFont.setBounds(10, 320 + 5*btnHeight, 200, btnHeight);
		contentPane.add(btnPlusFont);
		btnPlusFont.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnPlusFontAction(evt);
			}
		});
		//button to decrease a size of the text font
		JButton btnMinusFont = new JButton("-");
		btnMinusFont.setFont(btnFont);
		btnMinusFont.setBackground(UIManager.getColor("Button.background"));
		btnMinusFont.setBounds(220, 320 + 5*btnHeight, 200, btnHeight);
		contentPane.add(btnMinusFont);
		btnMinusFont.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnMinusFontAction(evt);
			}
		});
		
		//title for Aufgabe 5
		txtAufgabe5 = new JTextField();
		txtAufgabe5.setText("Aufgabe 5: Textausrichtung");
		txtAufgabe5.setHorizontalAlignment(SwingConstants.LEFT);
		txtAufgabe5.setForeground(Color.BLACK);
		txtAufgabe5.setFont(titleFont);
		txtAufgabe5.setEditable(false);
		txtAufgabe5.setBorder(null);
		txtAufgabe5.setBackground(null);
		txtAufgabe5.setColumns(10);
		txtAufgabe5.setBounds(10, 330 + 6*btnHeight, 410, 30);
		contentPane.add(txtAufgabe5);
		
		//button to change a text alignment to left
		JButton btnLeft = new JButton("Linksb�ndig");
		btnLeft.setFont(btnFont);
		btnLeft.setBackground(UIManager.getColor("Button.background"));
		btnLeft.setBounds(10, 360 + 6*btnHeight, 130, btnHeight);
		contentPane.add(btnLeft);
		btnLeft.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                UI_Copy.this.btnLeftAction(evt);
            }
        });	
		//button to change a text alignment to center
		JButton btnCenter = new JButton("Zentriert");
		btnCenter.setFont(btnFont);
		btnCenter.setBackground(UIManager.getColor("Button.background"));
		btnCenter.setBounds(150, 360 + 6*btnHeight, 130, btnHeight);
		contentPane.add(btnCenter);
		btnCenter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnCenterAction(evt);
			}
		});
		//button to change a text alignment to right
		JButton btnRight = new JButton("Rechtsb�ndig");
		btnRight.setBackground(UIManager.getColor("Button.background"));
		btnRight.setBounds(290, 360 + 6*btnHeight, 130, btnHeight);
		contentPane.add(btnRight);
		btnRight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent evt) {
				UI_Copy.this.btnRightAction(evt);
			}
		});
		
		//title for Aufgabe 6
		txtAufgabe6 = new JTextField();
		txtAufgabe6.setText("Aufgabe 6: Programm beenden");
		txtAufgabe6.setHorizontalAlignment(SwingConstants.LEFT);
		txtAufgabe6.setForeground(Color.BLACK);
		txtAufgabe6.setFont(titleFont);
		txtAufgabe6.setEditable(false);
		txtAufgabe6.setBorder(null);
		txtAufgabe6.setBackground(null);
		txtAufgabe6.setColumns(10);
		txtAufgabe6.setBounds(10, 370 + 7*btnHeight, 410, 30);
		contentPane.add(txtAufgabe6);
		
		//button to exit
		JButton btnExit = new JButton("Exit");
		btnExit.setFont(btnFont);
		btnExit.setBackground(UIManager.getColor("Button.background"));
		btnExit.setBounds(10, 400 + 7*btnHeight, 410, btnHeight);
		contentPane.add(btnExit);
		btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                UI_Copy.this.btnExitAction(evt);
            }
        });
	}
	
	//method to change background to red
	public void btnRedAction(final ActionEvent evt) {
		this.contentPane.setBackground(Color.RED);
	}
	//method to change background to green
	public void btnGreenAction(final ActionEvent evt) {
		this.contentPane.setBackground(Color.GREEN);
	}
	//method to change background to blue
	public void btnBlueAction(final ActionEvent evt) {
		this.contentPane.setBackground(Color.BLUE);
	}
	//method to change background to yellow
	public void btnYellowAction(final ActionEvent evt) {
		this.contentPane.setBackground(Color.YELLOW);
	}
	//method to change background to standard
	public void btnStandardAction(final ActionEvent evt) {
		this.contentPane.setBackground(new Color(15790320));
	}	
	//method to change background to chosen color
	public void btnChooseColorAction(final ActionEvent evt) {
		this.contentPane.setBackground(JColorChooser.showDialog(this, "Farbe w�hlen", Color.WHITE));
	}	
	
	//method to change a font of the text to Arial
	public void btnArialAction(final ActionEvent evt) {
		this.txtText.setFont(new Font("Arial", Font.PLAIN, textFontSize));
	}
	//method to change a font of the text to Comic Sans MS
	public void btnComicsansAction(final ActionEvent evt) {
		this.txtText.setFont(new Font("Comic Sans MS", Font.PLAIN, textFontSize));
	}
	//method to change a font of the text to Courier New
	public void btnCourierAction(final ActionEvent evt) {
		this.txtText.setFont(new Font("Courier New", Font.PLAIN, textFontSize));
	}
	//method to change a text
	public void btnChangeTextAction(final ActionEvent evt) {
		this.txtText.setText(this.txtNewText.getText());
	}
	//method to delete a text
	public void btnDeleteTextAction(final ActionEvent evt) {
		this.txtText.setText("");
	}
	
	//method to change a text to red
	public void btnRedTextAction(final ActionEvent evt) {
		this.txtText.setForeground(Color.RED);
	}
	//method to change a text to blue
	public void btnBlueTextAction(final ActionEvent evt) {
		this.txtText.setForeground(Color.BLUE);
	}
	//method to change a text to black
	public void btnBlackTextAction(final ActionEvent evt) {
		this.txtText.setForeground(Color.BLACK);
	}
	
	//method to increase a size of a text font
	public void btnPlusFontAction(final ActionEvent evt) {
		this.textFontSize += 1;
		String currentFont = this.txtText.getFont().getFontName();
		Font updatedFont = new Font(currentFont, Font.PLAIN, textFontSize);
		txtText.setFont(updatedFont);
	}
	//method to decrease a size of a text font
	public void btnMinusFontAction(final ActionEvent evt) {
		this.textFontSize -= 1;
		String currentFont = this.txtText.getFont().getFontName();
		Font updatedFont = new Font(currentFont, Font.PLAIN, textFontSize);
		txtText.setFont(updatedFont);
	}

	//method to change a text alignment to left
	public void btnLeftAction(final ActionEvent evt) {
		this.txtText.setHorizontalAlignment(SwingConstants.LEFT);
	}
	//method to change a text alignment to center
	public void btnCenterAction(final ActionEvent evt) {
		this.txtText.setHorizontalAlignment(SwingConstants.CENTER);
	}
	//method to change a text alignment to right
	public void btnRightAction(final ActionEvent evt) {
		this.txtText.setHorizontalAlignment(SwingConstants.RIGHT);
	}
	
	//method to end the programm
	public void btnExitAction(final ActionEvent evt) {
		System.exit(1);
	}
}
