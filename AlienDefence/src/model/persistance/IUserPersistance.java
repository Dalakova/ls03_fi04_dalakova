package model.persistance;

import model.User;

public interface IUserPersistance {

	User readUser(String username);
	
	void deleteUser(User user);
	
	void updateUser(User user);
	
	int createUser(User user);
	
	int lastID();

}