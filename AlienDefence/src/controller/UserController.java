package controller;

import model.User;
import model.persistance.IPersistance;
import model.persistance.IUserPersistance;

/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {

	private IUserPersistance userPersistance;
	
	public UserController(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
	}
	
	public UserController(IPersistance alienDefenceModel) {
		this.userPersistance = alienDefenceModel.getUserPersistance();
	}

	//Implementieren Sie die Methode createUser
	// Stellen Sie vor dem Eintrag in die Datenbank sicher, dass der Benutzer nicht sschon existiert.
	public void createUser(User user) {
		if(this.readUser(user.getLoginname(), user.getPassword()) == null) {
		this.userPersistance.createUser(user);
		}
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	//implementieren Sie die Methode readUser nach der JavaDoc Beschreibung.
	public User readUser(String username, String passwort) {
		User user = null;
		user = userPersistance.readUser(username);
		return user;
	}
	
	//Implementieren Sie die Methode changeUser.
	public void changeUser(User user) {
		this.userPersistance.updateUser(user);
	}
	
	//Implementieren Sie die Methode deleteUser.
	public void deleteUser(User user) {
		this.userPersistance.deleteUser(user);	
	}
	
	public int lastID() {
		int id = this.userPersistance.lastID();
		return id;
	}
	
	//Planen Sie wie die Methode checkPassword aussehen k�nnte. 
	//Sie d�rfen daf�r nur die gerade geschriebene Methode readUser(...) und die equals-Methode der Klasse String verwenden.
	public boolean checkPassword(String username, String passwort) {
		User user = this.readUser(username, passwort);
		if (user.getPassword().equals(passwort)) {
			return true;
		} else return false;
	}
}
