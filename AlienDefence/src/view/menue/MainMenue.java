package view.menue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.AlienDefenceController;
import controller.GameController;
import model.Level;
import model.persistanceDB.PersistanceDB;
import model.User;
import view.game.GameGUI;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Component;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import java.awt.ComponentOrientation;

public class MainMenue extends JFrame {

	private static final long serialVersionUID = 1L;
	private AlienDefenceController alienDefenceController;
	private JPanel contentPane;
	private JTextField tfdLogin;
	private JPasswordField pfdPassword;
	@SuppressWarnings("rawtypes")
	//private JComboBox cboLevelChooser;
	private List<Level> arrLevel;

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public MainMenue(AlienDefenceController alienDefenceController) {
		setMinimumSize(new Dimension(150, 530));

		this.alienDefenceController = alienDefenceController;
		
		//MainMenue und LevelChooser: das Fenster an die Bildschirmaufl�sung anpassen und �ber den gesamten Bildschirm gezogen starten
		//get the screen resolution
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		//setMinimumSize(screenSize);
		setMaximumSize(screenSize);
		//fullscreen mode
		setExtendedState(JFrame.MAXIMIZED_BOTH);

		// Allgemeine JFrame-Einstellungen
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 195, 518);
		contentPane = new JPanel();
		contentPane.setMinimumSize(new Dimension(150, 700));
		contentPane.setMaximumSize(new Dimension(150, 700));
		contentPane.setPreferredSize(new Dimension(150, 700));
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

		// �berschrift
		JLabel lblHeadline = new JLabel("ALIEN DEFENCE");
		lblHeadline.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblHeadline.setAlignmentY(Component.TOP_ALIGNMENT);
		//lblHeadline.setHorizontalAlignment(SwingConstants.CENTER);
		lblHeadline.setForeground(new Color(124, 252, 0));
		lblHeadline.setFont(new Font("Yu Gothic UI", Font.BOLD, 20));
		contentPane.add(lblHeadline);
		
		//Logo		
		JPanel pnlLogo = new JPanel();
		pnlLogo.setMinimumSize(new Dimension(150, 150));
		pnlLogo.setMaximumSize(new Dimension(150, 150));
		pnlLogo.setPreferredSize(new Dimension(150, 150));
		pnlLogo.setBackground(Color.BLACK);
		pnlLogo.setAlignmentX(Component.CENTER_ALIGNMENT);
		pnlLogo.setAlignmentY(Component.CENTER_ALIGNMENT);
		contentPane.add(pnlLogo);

		JLabel lblLogo = new JLabel("");
		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("./pictures/logo.png").getImage().getScaledInstance(140, 140, Image.SCALE_DEFAULT));
		lblLogo.setIcon(imageIcon);
		pnlLogo.add(lblLogo);

		// Alles unter dem Bild, Tabellenlayout mit einer Spalte
		JPanel pnlButtons = new JPanel();
		pnlButtons.setPreferredSize(new Dimension(150, 300));
		pnlButtons.setMinimumSize(new Dimension(150, 300));
		pnlButtons.setMaximumSize(new Dimension(150, 300));
		//pnlButtons.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		pnlButtons.setAlignmentX(Component.CENTER_ALIGNMENT);
		pnlButtons.setBackground(Color.BLACK);
		contentPane.add(pnlButtons);
		pnlButtons.setLayout(new GridLayout(0, 1, 0, 0));

		// Nutzername
		JLabel lblLogin = new JLabel("Login:");
		lblLogin.setForeground(Color.orange);
		pnlButtons.add(lblLogin);

		tfdLogin = new JTextField();
		pnlButtons.add(tfdLogin);
		tfdLogin.setColumns(10);

		// Passwort
		JLabel lblPassword = new JLabel("Passwort:");
		lblPassword.setForeground(Color.orange);
		pnlButtons.add(lblPassword);

		pfdPassword = new JPasswordField();
		pnlButtons.add(pfdPassword);

		//MainMenue: L�schen des Label "Level:" und des Drop-Down-Men�s
		// Levelauswahl
		//JLabel lblLevel = new JLabel("Level:");
		//lblLevel.setForeground(Color.orange);
		//pnlButtons.add(lblLevel);

		//Levelnamen auslesen
		arrLevel = this.alienDefenceController.getLevelController().readAllLevels();
		String[] arrLevelNames = getLevelNames(arrLevel);
		
		//MainMenue: L�schen des Label "Level:" und des Drop-Down-Men�s
		//Combobox erstellen
		//cboLevelChooser = new JComboBox(arrLevelNames);
		//pnlButtons.add(cboLevelChooser);
		
		// Empty Space
				JLabel lblEmpty = new JLabel("");
				pnlButtons.add(lblEmpty);
		
		//Spiel starten
		//Die Textfelder werden ausgewertet und das Passwort validiert, dann wird das Spiel gestartet
		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSpielen_Clicked(alienDefenceController, arrLevel);
			}
		});
		pnlButtons.add(btnSpielen);
		
		JButton btnTesten = new JButton("Testen");
		btnTesten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// Erstellt Modell von aktuellen Nutzer
				//User user = new User(1, "test", "pass");

				Thread t = new Thread("GameThread") {

					@Override
					public void run() {
						//MainMenue: �ndern des ActionListeners des Button Testen, dass statt der SpielGUI der LevelChooser erscheint
						new LeveldesignWindow(alienDefenceController.getLevelController(), alienDefenceController.getTargetController());
						//beim Button Spielen aus dem Hauptmen� soll nur der Button "Spielen" zu sehen sein
						LevelChooser.btnNewLevel.setVisible(false);
						LevelChooser.btnUpdateLevel.setVisible(false);
						LevelChooser.btnDeleteLevel.setVisible(false);
						LevelChooser.btnPlay.setVisible(true);
						
						//GameController gameController = alienDefenceController.startGame(arrLevel.get(0), user);
						//new GameGUI(gameController).start();
					}
				};
				t.start();
			}
		});
		pnlButtons.add(btnTesten);
		
		JButton btnHighscore = new JButton("Highscore");
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Highscore(alienDefenceController.getAttemptController(), 0);
			}
		});
		pnlButtons.add(btnHighscore);
		
		JButton btnLeveleditor = new JButton("Leveleditor");
		btnLeveleditor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new LeveldesignWindow(alienDefenceController.getLevelController(), alienDefenceController.getTargetController());
				//beim Button Leveleditor soll "Spielen" aber nicht zu sehen sein
				LevelChooser.btnPlay.setVisible(false);
				LevelChooser.btnNewLevel.setVisible(true);
				LevelChooser.btnUpdateLevel.setVisible(true);
				LevelChooser.btnDeleteLevel.setVisible(true);
			}
		});
		pnlButtons.add(btnLeveleditor);
		
		JButton btnCreateUser = new JButton("Create User");
		btnCreateUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame createUserWindow = new CreateUserWindow(alienDefenceController);
				createUserWindow.setVisible(true);
			}
		});
		pnlButtons.add(btnCreateUser);
		
		JButton btnBeenden = new JButton("Beenden");
		btnBeenden.setBackground(Color.GRAY);
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		pnlButtons.add(btnBeenden);

		//Logo		
		/*JPanel pnlLogo = new JPanel();
		pnlLogo.setPreferredSize(new Dimension(140, 140));
		pnlLogo.setBackground(Color.BLACK);
		contentPane.add(pnlLogo);

		JLabel lblLogo = new JLabel("");
		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("./pictures/logo.png").getImage().getScaledInstance(140, 140, Image.SCALE_DEFAULT));
		lblLogo.setIcon(imageIcon);
		pnlLogo.add(lblLogo);*/
	}

	private String[] getLevelNames(List<Level> arrLevel) {
		String[] arrLevelNames = new String[arrLevel.size()];

		for (int i = 0; i < arrLevel.size(); i++) {
			arrLevelNames[i] = arrLevel.get(i).getName(); // Array aus Arraylist erstellt
		}

		return arrLevelNames;
	}
	
	public void btnSpielen_Clicked(AlienDefenceController alienDefenceController, List<Level> arrLevel) {
		// User aus Datenbank holen
		//TODO B�ser Versto� gegen MVC - hier muss sp�ter nochmal nachgebessert werden
		User user = new PersistanceDB().getUserPersistance().readUser(tfdLogin.getText());

		// Spielstarten, wenn Nutzer existiert und Passwort �bereinstimmt
		if (user != null && user.getPassword().equals(new String(pfdPassword.getPassword()))) {

			Thread t = new Thread("GameThread") {
				@Override
				public void run() {

					GameController gameController = alienDefenceController.startGame(arrLevel.get(0), user);
					new GameGUI(gameController).start();
				}
			};
			t.start();
		} else {
			// Fehlermeldung - Zugangsdaten fehlerhaft
			JOptionPane.showMessageDialog(null, "Zugangsdaten nicht korrekt", "Fehler",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
