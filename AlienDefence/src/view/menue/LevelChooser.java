package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import model.persistance.IPersistance;
import model.persistanceDummy.PersistanceDummy;
import view.game.GameGUI;

import java.awt.Dimension;
import java.awt.Color;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	
	//to change the visibility
	static JButton btnPlay = new JButton("Spielen");
	static JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
	static JButton btnNewLevel = new JButton("Neues Level");
	static JButton btnDeleteLevel = new JButton("Level l\u00F6schen");

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(LevelController lvlControl, LeveldesignWindow leveldesignWindow) {
		setForeground(Color.WHITE);
		setBackground(Color.BLACK);
		this.lvlControl = lvlControl;
		this.leveldesignWindow = leveldesignWindow;

		setLayout(new BorderLayout());

		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		pnlButtons.setForeground(Color.WHITE);
		add(pnlButtons, BorderLayout.SOUTH);
		
		//LevelChooser: Erweiterung um den Button "Testen" mit Aufruf der SpielGUI
		//this.btnPlay = new JButton("Spielen");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnPlay_Clicked();
			}
		});
		pnlButtons.add(btnPlay);

		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		pnlButtons.add(btnNewLevel);

		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);
		
		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		spnLevels.setForeground(Color.ORANGE);
		spnLevels.setBackground(Color.BLACK);
		
		//color the table black
		spnLevels.getViewport().setBackground(Color.BLACK);
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setGridColor(Color.WHITE);
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setForeground(Color.ORANGE);
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		//color the header of table the sane as the table
		JTableHeader header = tblLevels.getTableHeader();
		header.setBackground(Color.BLACK);
		header.setForeground(Color.ORANGE);
		header.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}
	
	
	//LevelChooser: Erweiterung um den Button "Testen" mit Aufruf der SpielGUI
	public void btnPlay_Clicked() {
		
		//get level id from the table
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		
		//create temporary user 
		User user = new User(1, "test", "pass");
		
		//start game
		AlienDefenceController alienDefenceController = new AlienDefenceController(new PersistanceDummy());
		List<Level> arrLevel = alienDefenceController.getLevelController().readAllLevels();

		Thread t = new Thread("GameThread") {

			@Override
			public void run() {

				GameController gameController = alienDefenceController.startGame(arrLevel.get(level_id - 1), user);
				new GameGUI(gameController).start();
			}
		};
		t.start();
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
}
