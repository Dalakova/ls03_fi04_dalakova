package view.menue;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.AlienDefenceController;
import controller.UserController;
import model.User;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.Frame;

//TODO create a usermanagement
public class CreateUserWindow extends JFrame{
	private JTextField textField;
	private JTextField textField_1;
	
	//Erstellen Sie eine neue Oberfl�che mit einem Gridlayout f�r die Eingabe eines neuen Benutzers.
	public CreateUserWindow(AlienDefenceController alienDefenceController) {
		
		setMinimumSize(new Dimension(310, 440));
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		getContentPane().setMinimumSize(new Dimension(150, 530));
		setExtendedState(Frame.MAXIMIZED_BOTH);
		getContentPane().setBackground(Color.BLACK);
	
		JPanel fieldPanel = new JPanel();
		fieldPanel.setBackground(Color.BLACK);
		fieldPanel.setLayout(new GridLayout(0,2,5,5));
		fieldPanel.setMinimumSize(new Dimension(310, 400));
		fieldPanel.setMaximumSize(new Dimension(310, 400));
		fieldPanel.setPreferredSize(new Dimension(310, 400));
		fieldPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		fieldPanel.setVisible(true);
		
		getContentPane().add(fieldPanel);
		
		JLabel lblNewUser = new JLabel("New User");
		lblNewUser.setMaximumSize(new Dimension(100,20));
		lblNewUser.setFont(new Font("Arial", Font.BOLD, 18));
		lblNewUser.setForeground(Color.GREEN);
		fieldPanel.add(lblNewUser);
		
		JLabel dummy = new JLabel("");
		dummy.setMaximumSize(new Dimension(100,20));
		fieldPanel.add(dummy);
		
		JLabel lblLogin = new JLabel("Login:");
		lblLogin.setMaximumSize(new Dimension(100, 20));
		lblLogin.setForeground(Color.orange);
		fieldPanel.add(lblLogin);

		JTextField tfdLogin = new JTextField();
		tfdLogin.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(tfdLogin);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setMaximumSize(new Dimension(100, 20));
		lblPassword.setForeground(Color.orange);
		fieldPanel.add(lblPassword);
		
		JTextField tfdPassword = new JTextField();
		tfdPassword.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(tfdPassword);
		
		JLabel lblFirstname = new JLabel("First name:");
		lblFirstname.setMaximumSize(new Dimension(100, 20));
		lblFirstname.setForeground(Color.orange);
		fieldPanel.add(lblFirstname);
		
		JTextField tfdFirstname = new JTextField();
		tfdFirstname.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(tfdFirstname);
		
		JLabel lblSurname = new JLabel("Surname:");
		lblSurname.setMaximumSize(new Dimension(100, 20));
		lblSurname.setForeground(Color.orange);
		fieldPanel.add(lblSurname);
		
		JTextField tfdSurname = new JTextField();
		tfdSurname.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(tfdSurname);
		
		JLabel lblBirthday = new JLabel("Birthday (yyyy-mm-dd):");
		lblBirthday.setMaximumSize(new Dimension(100, 20));
		lblBirthday.setForeground(Color.orange);
		fieldPanel.add(lblBirthday);
		
		JTextField tfdBirthday = new JTextField();
		tfdBirthday.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(tfdBirthday);
		
		JLabel lblStreet = new JLabel("Street:");
		lblStreet.setMaximumSize(new Dimension(100, 20));
		lblStreet.setForeground(Color.orange);
		fieldPanel.add(lblStreet);
		
		JTextField tfdStreet = new JTextField();
		tfdStreet.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(tfdStreet);
		
		JLabel lblHouse = new JLabel("House:");
		lblHouse.setMaximumSize(new Dimension(100, 20));
		lblHouse.setForeground(Color.orange);
		fieldPanel.add(lblHouse);
		
		JTextField tfdHouse = new JTextField();
		tfdHouse.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(tfdHouse);
		
		JLabel lblPostal = new JLabel("Postal code:");
		lblPostal.setMaximumSize(new Dimension(100, 20));
		lblPostal.setForeground(Color.orange);
		fieldPanel.add(lblPostal);
		
		JTextField tfdPostal = new JTextField();
		tfdPostal.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(tfdPostal);
		
		JLabel lblCity = new JLabel("City:");
		lblCity.setMaximumSize(new Dimension(100, 20));
		lblCity.setForeground(Color.orange);
		fieldPanel.add(lblCity);
		
		JTextField tfdCity = new JTextField();
		tfdCity.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(tfdCity);
		
		JLabel lblSalary = new JLabel("Salary expectations (int):");
		lblSalary.setMaximumSize(new Dimension(100, 20));
		lblSalary.setForeground(Color.orange);
		fieldPanel.add(lblSalary);
		
		JTextField tfdSalary = new JTextField();
		tfdSalary.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(tfdSalary);
		
		JLabel lblMarital = new JLabel("Marital status:");
		lblMarital.setMaximumSize(new Dimension(100, 20));
		lblMarital.setForeground(Color.orange);
		fieldPanel.add(lblMarital);
		
		JTextField tfdMarital = new JTextField();
		tfdMarital.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(tfdMarital);
		
		JLabel lblGrade = new JLabel("Final grade (float):");
		lblGrade.setMaximumSize(new Dimension(100, 20));
		lblGrade.setForeground(Color.orange);
		fieldPanel.add(lblGrade);
		
		JTextField tfdGrade = new JTextField();
		tfdGrade.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(tfdGrade);
		
		JLabel dummy2 = new JLabel("");
		dummy2.setMaximumSize(new Dimension(100,20));
		fieldPanel.add(dummy2);
		
		JButton btnSave = new JButton("Speichern");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//Diese Oberfl�che soll nur die Nutzererstellung vornehmen, wenn der Datensatz komplett eingegeben wurde.
				if(tfdLogin.getText().isEmpty() || tfdPassword.getText().isEmpty() || tfdFirstname.getText().isEmpty() || tfdSurname.getText().isEmpty() ||
						tfdBirthday.getText().isEmpty() || tfdStreet.getText().isEmpty() || tfdHouse.getText().isEmpty() || tfdPostal.getText().isEmpty() ||
						tfdCity.getText().isEmpty() || tfdSalary.getText().isEmpty() || tfdMarital.getText().isEmpty() || tfdGrade.getText().isEmpty()) {
					
					JOptionPane.showMessageDialog(null, "F�llen Sie alle Felder aus", "Fehler", JOptionPane.ERROR_MESSAGE);
				}
				else {
					
					UserController userController = alienDefenceController.getUserController(); 
					
					int p_user_id = userController.lastID() + 1;
					String firstname = tfdFirstname.getText();
					String surname = tfdSurname.getText();
					LocalDate birthday = LocalDate.parse(tfdBirthday.getText());
					String street = tfdStreet.getText();
					String house= tfdHouse.getText();
					String postal = tfdPostal.getText();
					String city = tfdCity.getText();
					int salary = Integer.parseInt(tfdSalary.getText());
					String marital = tfdMarital.getText();
					double grade = Double.parseDouble(tfdGrade.getText());
					String login = tfdLogin.getText();
					String password = tfdPassword.getText();
			
					User user = new User(p_user_id, firstname, surname, birthday, street, house, postal, city, login, password, salary, marital, grade);
					userController.createUser(user);
					
					dispose();
				}
			}
		});
		btnSave.setMaximumSize(new Dimension(100, 20));
		fieldPanel.add(btnSave);
	}

}
